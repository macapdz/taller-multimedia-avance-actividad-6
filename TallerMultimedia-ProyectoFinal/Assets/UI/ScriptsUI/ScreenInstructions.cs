﻿using System.Collections;

using System.Collections.Generic;

using UnityEngine;



public class ScreenInstructions : MonoBehaviour {


	public GameObject ScreenPlay;

	public void GoToScreenPlay(){

		//Busco la pantalla ScreenInstructions
		GameObject ScreenInstructions = GameObject.Find("Canvas/ScreenInstructions");

		//Elimino la pantalla ScreenInstructions
		GameObject.Destroy(ScreenInstructions);

		//BuscoalGameObject Canvas
		GameObject canvas = GameObject.Find("Canvas");

		//Creo una copia de prefab ScreenPlay
		GameObject.Instantiate(ScreenPlay, canvas.transform);
		}
}

