﻿using UnityEngine;

public class ScreenMenu : MonoBehaviour {

	public GameObject ScreenMision;

	public void GoToScreenMision(){

		//Busco la pantalla ScreenMenu
		GameObject ScreenMenu = GameObject.Find("Canvas/ScreenMenu");

		//Elimino la pantalla ScreenMenu
		GameObject.Destroy(ScreenMenu);

		//BuscoalGameObject Canvas
		GameObject canvas = GameObject.Find("Canvas");

		//Creo una copia de prefab ScreenMision
		GameObject.Instantiate(ScreenMision, canvas.transform);
    }               

	public GameObject ScreenExit;

	public void GoToScreenExit(){

		//Busco la pantalla ScreenMenu
		GameObject ScreenMenu = GameObject.Find ("Canvas/ScreenMenu");

		//Elimino la pantalla ScreenMenu
		GameObject.Destroy (ScreenMenu);

		//BuscoalGameObject Canvas
		GameObject canvas = GameObject.Find ("Canvas");

		//Creo una copia de prefab ScreenExit
		GameObject.Instantiate (ScreenExit, canvas.transform);

	}

	public GameObject ScreenConfiguration;

	public void GoToScreenConfiguration(){

		//Busco la pantalla ScreenMenu
		GameObject ScreenMenu = GameObject.Find ("Canvas/ScreenMenu");

		//Elimino la pantalla ScreenMenu
		GameObject.Destroy (ScreenMenu);

		//BuscoalGameObject Canvas
		GameObject canvas = GameObject.Find ("Canvas");

		//Creo una copia de prefab ScreenConfiguration
		GameObject.Instantiate (ScreenConfiguration, canvas.transform);

	}

}