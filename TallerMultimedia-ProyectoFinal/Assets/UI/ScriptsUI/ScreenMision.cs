﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenMision : MonoBehaviour {
	

	public GameObject ScreenInstructions;

	public void GoToScreenInstructions(){

		//Busco la pantalla ScreenMision
		GameObject ScreenExit = GameObject.Find ("Canvas/ScreenMision");

		//Elimino la pantalla ScreenMision
		GameObject.Destroy (ScreenMision);

		//BuscoalGameObject Canvas
		GameObject canvas = GameObject.Find ("Canvas");

		//Creo una copia de prefab ScreenInstructions
		GameObject.Instantiate (ScreenInstructions, canvas.transform);

	}
}
