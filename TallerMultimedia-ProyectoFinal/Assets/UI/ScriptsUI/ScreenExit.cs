﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenExit : MonoBehaviour {

	public GameObject ScreenMenu;

	public void GoToScreenMenu(){

		//Busco la pantalla ScreenExit
		GameObject ScreenExit = GameObject.Find ("Canvas/ScreenExit");

		//Elimino la pantalla ScreenExit
		GameObject.Destroy (ScreenExit);

		//BuscoalGameObject Canvas
		GameObject canvas = GameObject.Find ("Canvas");

		//Creo una copia de prefab ScreenMenu
		GameObject.Instantiate (ScreenMenu, canvas.transform);
	}
}
